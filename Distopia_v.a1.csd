<Cabbage> bounds(0, 0, 0, 0)
form caption("Untitled") size(586, 300), colour(61, 68, 96), pluginid("def1")

image bounds(-2, 2, 600, 300) colour(36, 58, 77, 255) file("..\..\visual art\parts\water dark blue.png")

; part A
image bounds(6, 4, 329, 108)  colour(0, 0, 0, 67)  outlinethickness(1)   outlinecolour(255, 255, 255, 0)
checkbox bounds(26, 34, 20, 16) channel("switchA") colour:0(63, 77, 106, 255) corners(0) colour:1(89, 161, 255, 255) fontcolour:0(183, 0, 0, 255) radiogroup("0")

label bounds(12, 12, 50, 15) text("PartA") fontcolour(255, 255, 255, 255)
rslider bounds(62, 8, 56, 52) channel("detuneA"), range(0, 5, 2, 1, 0.001) text("Detune") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255)
rslider bounds(116, 8, 56, 52) channel("octA"), range(-4, 4, 0, 1, 1) text("Oct") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255)
rslider bounds(170, 8, 56, 52) channel("voicesA"), range(1, 13, 7, 1, 1) text("Voices") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255) markercolour(80, 80, 80, 0)
rslider bounds(224, 8, 56, 52) channel("panA"), range(1, 7, 7, 1, 1) text("Pan") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255) markercolour(80, 80, 80, 0)
rslider bounds(278, 8, 56, 52) channel("phaseA"), range(0, 1, 1, 1, 0.001) text("Phase") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255)kKFDetun
rslider bounds(278, 60, 56, 52) channel("keyFollowDetuneA"), range(0, 2, 1.2, 1, 0.0003) text("KF Detune") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255) markercolour(80, 80, 80, 0)
rslider bounds(224, 60, 56, 52) channel("partDetA"), range(1, 7, 7, 1, 1) text("Part Det") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255) markercolour(80, 80, 80, 0)
rslider bounds(170, 60, 56, 52) channel("gainA"), range(0, 1, 1, 1, 0.001) text("Gain") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255)
combobox bounds(89, 62, 80, 22) channel("waveA") text("Saw", "Square") colour(14, 104, 123, 93) value("1")
combobox bounds(89, 86, 80, 22) channel("unisonTypeA") text("Multiply", "Random", "") colour(14, 104, 123, 93)
label bounds(13, 66, 80, 12) text("Waveform") fontcolour(255, 255, 255, 255) align("left")
label bounds(13, 90, 80, 12) text("Unison type") fontcolour(255, 255, 255, 255) align("left")

; part B
image bounds(6, 116, 329, 108)  colour(0, 0, 0, 67)  outlinethickness(1)   outlinecolour(255, 255, 255, 0)

checkbox bounds(26, 144, 20, 16) channel("switchB") colour:0(63, 77, 106, 255) corners(0) colour:1(89, 161, 255, 255) fontcolour:0(183, 0, 0, 255) 

label bounds(12, 124, 50, 15) text("PartB") fontcolour(255, 255, 255, 255)
rslider bounds(62, 118, 56, 52) channel("detuneB"), range(0, 5, 2, 1, 0.001) text("Detune") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255)
rslider bounds(116, 118, 56, 52) channel("octB"), range(-4, 4, 0, 1, 1) text("Oct") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255)
rslider bounds(170, 118, 56, 52) channel("voicesB"), range(1, 13, 7, 1, 1) text("Voices") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255) markercolour(80, 80, 80, 0)
rslider bounds(224, 118, 56, 52) channel("panB"), range(1, 7, 7, 1, 1) text("Pan") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255) markercolour(80, 80, 80, 0)
rslider bounds(278, 118, 56, 52) channel("phaseB"), range(0, 1, 1, 1, 0.001) text("Phase") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255)
rslider bounds(278, 170, 56, 52) channel("keyFollowDetuneB"), range(0, 2, 1.2, 1, 0.0003) text("KF Detune") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255) markercolour(80, 80, 80, 0)
rslider bounds(224, 170, 56, 52) channel("partDetB"), range(1, 7, 7, 1, 1) text("Part Det") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255) markercolour(80, 80, 80, 0)
rslider bounds(170, 170, 56, 52) channel("gainB"), range(0, 1, 1, 1, 0.001) text("Gain") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255)
combobox bounds(89, 172, 80, 22) channel("waveB") text("Saw", "Square") colour(14, 104, 123, 93) value("1")
combobox bounds(88, 196, 80, 22) channel("unisionTypeB") text("Multiply", "Random", "", "") colour(14, 104, 123, 93)
label bounds(13, 176, 80, 12) text("Waveform") fontcolour(255, 255, 255, 255) align("left")
label bounds(13, 200, 80, 12) text("Unison type") fontcolour(255, 255, 255, 255) align("left")

; adsr
image bounds(340, 5, 118, 127)  colour(0, 0, 0, 67)  outlinethickness(1)   outlinecolour(255, 255, 255, 0)
rslider bounds(342, 80, 56, 52) channel("mAtk"), range(0.01, 1, 0.01, 1, 0.001) text("Atk") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255)
rslider bounds(398, 30, 56, 52) channel("mSus") range(0, 1, 0.4, 1, 0.001) text("Sus") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255)
rslider bounds(342, 30, 56, 52) channel("mDec") range(0, 1, 0.25, 1, 0.001) text("Dec") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255)
rslider bounds(398, 80, 56, 52) channel("mRel") range(0, 1, 0.4, 1, 0.001) text("Rel") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255)
label bounds(374, 12, 50, 15) text("Adsr") fontcolour(255, 255, 255, 255)

; keyboard
keyboard bounds(-82, 266, 675, 36) whitenotecolour(0, 0, 0, 0) blacknotecolour(255, 255, 255, 128) arrowbackgroundcolour(255, 255, 255, 63) alpha(0.52)

; master
image bounds(463, 5, 118, 127)  colour(0, 0, 0, 67)  outlinethickness(1)   outlinecolour(255, 255, 255, 0)
rslider bounds(464, 28, 56, 52) channel("gainM") range(0, 0.5, 0.1, 1, 0.001) text("m Gain") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255)
rslider bounds(464, 80, 56, 52) channel("analogAmnt") range(0, 0.02, 0.005, 1, 0.0001) text("Analog") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255)
rslider bounds(520, 80, 56, 52) channel("detuneM") range(0, 4, 1, 1, 0.001) text("Detune") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255) 
rslider bounds(518, 28, 56, 52) channel("color") range(0, 0.5, 0.1, 1, 0.001) text("Color") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255) markercolour(80, 80, 80, 0)
label bounds(494, 12, 50, 15) text("Master") fontcolour(255, 255, 255, 255)

; filter
image bounds(340, 136, 240, 128)  colour(0, 0, 0, 67)  outlinethickness(1)   outlinecolour(255, 255, 255, 0)
rslider bounds(340, 160, 56, 52) channel("fatk") range(0.01, 1, 0.01, 1, 0.001) text("Atk") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255) markercolour(80, 80, 80, 0)
rslider bounds(394, 212, 56, 52) channel("frel") range(0, 1, 0.2, 1, 0.001) text("Rel") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255) markercolour(80, 80, 80, 0)
rslider bounds(394, 160, 56, 52) channel("fsus") range(0, 1, 1, 1, 0.001) text("Sus") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255) markercolour(80, 80, 80, 0)
rslider bounds(340, 212, 56, 52) channel("fdec") range(0, 1, 1, 1, 0.001) text("Dec") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255) markercolour(80, 80, 80, 0)
rslider bounds(450, 160, 56, 52) channel("cutoff") range(0, 20000, 20000, 1, 0.001) text("cutoff") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255)
rslider bounds(450, 212, 56, 52) channel("resonance") range(0, 20000, 20000, 1, 0.001) text("Resonance") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255) markercolour(80, 80, 80, 0)
rslider bounds(506, 212, 56, 52) channel("fMix") range(0, 20000, 20000, 1, 0.001) text("mix") colour(207, 207, 207, 255) outlinecolour(0, 0, 0, 0) textcolour(255, 255, 255, 255) trackercolour(14, 104, 123, 255) markercolour(80, 80, 80, 0)
label bounds(372, 144, 50, 15) text("Filter") fontcolour(255, 255, 255, 255)
combobox bounds(510, 166, 61, 26) text("LP 6db", "LP 12db") colour(14, 104, 123, 93) fontcolour(255, 255, 255, 0)

; synth label
image bounds(6, 228, 329, 35)  colour(0, 0, 0, 67)  outlinethickness(1)   outlinecolour(255, 255, 255, 0)
label bounds(13, 232, 187, 26) text("Distopia v.a1") fontcolour(255, 255, 255, 255) align("left") 
label bounds(160, 246, 183, 10) text("gitlab.com/mPlagge/mysynths")
label bounds(512, 197, 46, 11) text("Type") fontcolour(255, 255, 255, 255)

</Cabbage>
<CsoundSynthesizer>
<CsOptions>
-n -d -+rtmidi=NULL -M0 -m0d --midi-key-cps=4 --midi-velocity-amp=5
</CsOptions>
<CsInstruments>
ksmps = 32
nchnls = 2
0dbfs = 1

;------------------------------------------------------------------------------------------------------------

; return the imode value for vco2 oscilator based on a combobox index
opcode GetVco2WaveNum, i, i
    iWave xin
    if (iWave==1) then
        iVco2Wave = 0 // saw
    elseif (iWave==2) then
        iVco2Wave = 10 // square  
    endif       
    xout iVco2Wave  
endop

; return value to multiply a note with to shift the octave up or down
opcode OctaveMultiplyer, i, i
    iOctaveShift xin 
    xout (2^ iOctaveShift)
endop

; array to multiply voice detunes with to achive unision
giDetMultiplyUneven[] fillarray 0, -1, 1, -2, 2, -3, 3, -4, 4, -5, 5, -6, 6
giDetMultiplyEven[] fillarray -1, 1, -2, 2, -3, 3, -4, 4, -5, 5, -6, 6, -7, 7

; return bank with detuned oscilator voices, creating a supersaw (or square)
opcode SuperSaw, a, kkiiiiiik
    kAmp, kFreq, iVoices, iCnt, iOctiveMultiplyer, iDetune, iPhaseRange, iVco2WaveNum, kKFDetune xin
        
    kKFMultiplyer = kFreq^kKFDetune * 0.0009 ; TODO key tracking detune formula is very bad
    printk2 kKFMultiplyer

    ; TODO only initiate this once somehow
    if (iVoices % 2 == 0) then
        iDetMultiply[] = giDetMultiplyUneven
    else
        iDetMultiply[] = giDetMultiplyUneven
    endif

    iPhase random 0, iPhaseRange
    
    ; TODO strange static high note when changing voices
    aSig vco2 kAmp/iVoices, iOctiveMultiplyer* kFreq + iDetune*iDetMultiply[iCnt]*kKFMultiplyer, iVco2WaveNum, 0, iPhase

    iCnt = iCnt+1 
    if iCnt < iVoices then 
       aVcoBank SuperSaw iVoices, kFreq, iVoices, iCnt, iOctiveMultiplyer, iDetune, iPhaseRange, iVco2WaveNum, kKFDetune
    endif 
    xout aSig + aVcoBank
endop

;------------------------------------------------------------------------------------------------------------

instr 1
    
    iFreq, iVelocity = p4, p5    
    kMEnv madsr chnget:i("mAtk"), chnget:i("mDec"), chnget:i("mSus"), chnget:i("mRel"); master
    kFEnv madsr chnget:i("fAtk"), chnget:i("fDec"), chnget:i("fSus"), chnget:i("fRel"); filter
      
    aOutPartA SuperSaw\
        iVelocity, iFreq, chnget:i("voicesA"), 0, OctaveMultiplyer:i(chnget:i("octA")),
        chnget:i("detuneA")*chnget:i("detuneM"), chnget:i("phaseA"), GetVco2WaveNum:i(chnget:i("waveA")),
        chnget:i("keyFollowDetuneA")
 
    aOutPartB SuperSaw\
        iVelocity, iFreq, chnget:i("voicesB"), 0, OctaveMultiplyer:i(chnget:i("octB")),
        chnget:i("detuneB")*chnget:i("detuneM"),chnget:i("phaseB"), GetVco2WaveNum:i(chnget:i("waveB")),
        chnget:i("keyFollowDetuneB")

    outs aOutPartA*kMEnv*chnget:i("gainM")*chnget:i("gainA"),\
         aOutPartB*kMEnv*chnget:i("gainM")*chnget:i("gainB")

endin

</CsInstruments>
<CsScore>
;causes Csound to run for about 7000 years...
f0 z
</CsScore>
</CsoundSynthesizer>
